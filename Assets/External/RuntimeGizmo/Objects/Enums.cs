namespace RuntimeGizmos
{
    public enum TransformSpace
    {
        Global,
        Local
    }

    public enum Axis
    {
        None,
        X,
        Y,
        Z,
        Any
    }
}
