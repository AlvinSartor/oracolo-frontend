﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using AdvancedGraph;
using AdvancedGraph.Interfaces;
using Assets.Scripts;
using Assets.UI;
using CodeAnalysis.ProjectMapping.Interfaces;
using Extensions;
using JetBrains.Annotations;
using UnityEngine;

public class GraphBuilder : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private GameObject m_AssemblyPanelPrefab;
    [SerializeField] private CameraController m_CameraController;
#pragma warning restore 649    

    [NotNull] public CodeAnalysisFacade Plug { get; private set; }
    [NotNull] public Camera m_MainCamera { get; private set; }

    private Graph<ICsprojDescriptor> m_DirectDependencies;
    private Graph<ICsprojDescriptor> m_InverseDependencies;

    private Dictionary<ICsprojDescriptor, AssemblyModel> m_AddedNodes;
    private AssembliesHighlighter m_AssembliesHighlighter;
    private AssembliesSorter m_AssembliesSorter;

    public void Start()
    {
        Plug = new CodeAnalysisFacade();
        m_DirectDependencies = new Graph<ICsprojDescriptor>();
        m_InverseDependencies = new Graph<ICsprojDescriptor>();

        m_AddedNodes = new Dictionary<ICsprojDescriptor, AssemblyModel>();
        m_AssembliesHighlighter = new AssembliesHighlighter(csproj => m_AddedNodes[csproj]);
        m_AssembliesSorter = new AssembliesSorter(csproj => m_AddedNodes[csproj]);

        m_MainCamera = m_CameraController.GetComponent<Camera>();
    }

    public bool VerifyAssemblyName([NotNull] string assemblyName) => Plug.VerifyAssemblyName(assemblyName);

    public bool VerifyFileName([NotNull] string fileName) => Plug.VerifyFileName(fileName);


    [NotNull]
    public AssemblyModel AddAssemblyUsingPath([NotNull] string assemblyPath)
    {
        ICsprojDescriptor csproj = Plug.GetCsprojByPath(assemblyPath);
        return AddAssembly(csproj);
    }

    [NotNull]
    public AssemblyModel AddAssemblyUsingName([NotNull] string assemblyName, bool select = false)
    {
        ICsprojDescriptor csproj = Plug.GetCsprojByName(assemblyName);
        return AddAssembly(csproj, select: select);
    }

    [NotNull]
    private AssemblyModel AddAssembly([NotNull] ICsprojDescriptor csproj, Vector3 centerPosition = new Vector3(), bool select = false)
    {
        if (!m_AddedNodes.ContainsKey(csproj))
        {
            var model = Instantiate(m_AssemblyPanelPrefab, GetRandomPosition() + centerPosition, Quaternion.identity).GetComponent<AssemblyModel>();
            model.Initialize(this, csproj);

            m_AddedNodes.Add(csproj, model);
            m_DirectDependencies.Add(csproj);
            m_InverseDependencies.Add(csproj);
        }

        if (select)
        {
            m_AddedNodes[csproj].Select();
            m_CameraController.SetNewTarget(m_AddedNodes[csproj].transform);
        }

        return UpdateAssembly(csproj);
    }

    private static Vector3 GetRandomPosition()
    {
        const float range = 500;
        float GetCoord() => UnityEngine.Random.Range(-range, range);

        return new Vector3(GetCoord(), GetCoord(), GetCoord());
    }

    [NotNull]
    private AssemblyModel UpdateAssembly([NotNull] ICsprojDescriptor csproj)
    {
        AssemblyModel nodeModel = m_AddedNodes[csproj];

        Node<ICsprojDescriptor> directNode = m_DirectDependencies.GetNode(csproj);
        Node<ICsprojDescriptor> inverseNode = m_InverseDependencies.GetNode(csproj);

        IEnumerable<ICsprojDescriptor> dependenciesOfCsproj = Plug.GetDependenciesOf(csproj).Where(x => m_DirectDependencies.Contains(x));
        dependenciesOfCsproj.ForEach(x => CreateDirectConnection(x, directNode, inverseNode, nodeModel));

        IEnumerable<ICsprojDescriptor> dependantsByCsproj = Plug.GetDependentantsOf(csproj).Where(x => m_DirectDependencies.Contains(x));
        dependantsByCsproj.ForEach(x => CreateIndirectConnection(x, directNode, inverseNode, nodeModel));

        return m_AddedNodes[csproj];
    }

    public void RemoveAssembly([NotNull] ICsprojDescriptor csproj)
    {
        if (!m_AddedNodes.ContainsKey(csproj))
        {
            throw new ArgumentException($"Impossible to remove csproj. {csproj.CsprojName} has not been found.");
        }

        // fetching exiting and entering connections
        ImmutableList<ICsprojDescriptor> directConnections = m_DirectDependencies[csproj].CurrentNeighbors.Select(x => x.Value).ToImmutableList();
        ImmutableList<ICsprojDescriptor> inverseConnections = m_InverseDependencies[csproj].CurrentNeighbors.Select(x => x.Value).ToImmutableList();

        // removing the connections from the models
        AssemblyModel model = m_AddedNodes[csproj];
        model.RemoveAllConnections();

        // removing the connections from the direct graph
        Node<ICsprojDescriptor> directNode = m_DirectDependencies.GetNode(csproj);
        directConnections.ForEach(conn => directNode.RemoveConnection(m_DirectDependencies[conn]));
        inverseConnections.ForEach(conn => m_DirectDependencies.GetNode(conn).RemoveConnection(directNode));

        // removing the connections from the inverse graph
        Node<ICsprojDescriptor> inverseNode = m_InverseDependencies.GetNode(csproj);
        inverseConnections.ForEach(conn => inverseNode.RemoveConnection(m_InverseDependencies[conn]));
        directConnections.ForEach(conn => m_InverseDependencies.GetNode(conn).RemoveConnection(inverseNode));

        // removing the node from the graph
        m_DirectDependencies.Remove(csproj);
        m_InverseDependencies.Remove(csproj);

        // removing model
        m_AddedNodes.Remove(csproj);
        Destroy(model.gameObject);
    }

    public void RemoveAllAssemblies()
    {
        IEnumerator SlowRemotion()
        {
            var nodes = m_AddedNodes.Select(x => x.Key).ToImmutableList();
            foreach (var descriptor in nodes)
            {
                RemoveAssembly(descriptor);
                yield return null;
            }
        }

        StartCoroutine(SlowRemotion());
    }

    /// <summary>
    /// Connecting NodeModel -> X and updating the two graphs.
    /// </summary>
    private void CreateDirectConnection(
        [NotNull] ICsprojDescriptor x,
        [NotNull] Node<ICsprojDescriptor> directNode,
        [NotNull] Node<ICsprojDescriptor> inverseNode,
        [NotNull] AssemblyModel nodeModel)
    {
        directNode.TryAddConnection(m_DirectDependencies.GetNode(x));
        m_InverseDependencies.GetNode(x).TryAddConnection(inverseNode);

        if (!nodeModel.IsConnectedTo(m_AddedNodes[x]))
        {
            nodeModel.AddConnectionFromSource(m_AddedNodes[x]);
        }
    }

    /// <summary>
    /// Connecting X -> NodeModel and updating the two graphs.
    /// </summary>
    private void CreateIndirectConnection(
        [NotNull] ICsprojDescriptor x,
        [NotNull] Node<ICsprojDescriptor> directNode,
        [NotNull] Node<ICsprojDescriptor> inverseNode,
        [NotNull] AssemblyModel nodeModel)
    {
        inverseNode.TryAddConnection(m_InverseDependencies.GetNode(x));
        m_DirectDependencies.GetNode(x).TryAddConnection(directNode);

        if (!m_AddedNodes[x].IsConnectedTo(nodeModel))
        {
            m_AddedNodes[x].AddConnectionFromSource(nodeModel);
        }
    }

    public void ExpandInBlock([NotNull] AssemblyModel model)
    {
        ResetHightlightChanges();
        ICsprojDescriptor csproj = model.Csproj;
        IEnumerable<ICsprojDescriptor> correlatedCsprojs = Plug.GetDependenciesOf(csproj).Concat(Plug.GetDependentantsOf(csproj));

        IEnumerator SlowExpansion()
        {
            foreach (var descriptor in correlatedCsprojs)
            {
                yield return AddAssembly(descriptor, model.transform.position + Vector3.forward * 800);
            }
        }

        StartCoroutine(SlowExpansion());
    }

    /// <summary>
    /// Higlights some assemblies, changing their position and opacity.
    /// </summary>
    /// <param name="startingPoint">The starting point.</param>
    /// <param name="exiting">if set to <c>true</c> the [exiting] connections from the starting point will be highlighted;
    /// otherwise the [entering] connections will be highlighted.</param>
    /// <param name="depth">The maximum depth the highlight will get to.</param>
    public void SelectiveHighlight([NotNull] ICsprojDescriptor startingPoint, bool exiting, int depth)
    {
        ResetHightlightChanges();

        depth = Mathf.Clamp(depth, 0, Settings.MaxDepth);
        IGraph<ICsprojDescriptor> graph = exiting ? m_DirectDependencies : m_InverseDependencies;
        m_AssembliesHighlighter.SelectiveHighlight(startingPoint, graph, exiting, depth);
    }

    public void DependenciesSorting(bool exiting)
    {
        ResetHightlightChanges();
        
        int CountDependencies(ICsprojDescriptor csproj) => exiting 
            ? Plug.CountDependenciesOf(csproj) 
            : Plug.CountDependantsOn(csproj);

        m_AssembliesSorter.SortByDependencies(m_AddedNodes.Values, CountDependencies, m_CameraController.GetTargetPosition());
    }

    /// <summary>
    /// Resets the hightlight state.
    /// </summary>
    public void ResetHightlightChanges()
    {
        m_AssembliesHighlighter.ResetHightlightChanges(m_AddedNodes.Values);
        m_AssembliesSorter.ResetHightlightChanges(m_AddedNodes.Values);
    }
}