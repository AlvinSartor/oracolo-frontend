﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using JetBrains.Annotations;
using UnityEngine;

public sealed class Settings
{
    [NotNull]
    private static string GetSettingsFilePath() => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OracoloSettings.xml");

    // SLN path
    [NotNull] public static readonly string SlnPath = $@"D:\Development\Projects\Starfix NG\Source\StarfixDevelopment.sln";

    // Assemblies:

    public static readonly float AlphaValueForUnselectedAssembly = 0.05f;
    public static readonly float AlphaValueForSelectedAssembly = 1f;
    public static readonly float AssemblyModelHeight = 70;
    public static readonly float AssemblyModelWidth = 180;
    public static Vector2 Bounds() => new Vector2(AssemblyModelWidth, AssemblyModelHeight);

    public static readonly int MaxDepth = 10;
    public static readonly float PerDepth = 500;
    public static readonly float PerHeight = 200;
    public static readonly float WhenUnselected = 2000;

    // Connections:

    public static readonly float ConnectionWidth = 1;
    public static readonly float HighlightedConnectionWidth = 3;                  
    public static readonly float ArrowBaseSize = 15;
    public static readonly float ArrowLength = 15;
    public static readonly float DistanceBetweenArrows = 200;

    // Sorting: 

    public static readonly int DependenciesSortingGroupThreshold = 20;

    static Settings()
    {
        string settingsFilePath = GetSettingsFilePath();
        if (File.Exists(settingsFilePath))
        {
            List<XElement> descendants = XDocument.Load(settingsFilePath).Descendants().Descendants().ToList();

            SlnPath = TryGetStringValue(descendants, nameof(SlnPath)) ?? SlnPath;

            AlphaValueForUnselectedAssembly = TryGetFloatValue(descendants, nameof(AlphaValueForUnselectedAssembly)) ?? AlphaValueForUnselectedAssembly;
            AlphaValueForSelectedAssembly = TryGetFloatValue(descendants, nameof(AlphaValueForSelectedAssembly)) ?? AlphaValueForSelectedAssembly;
            MaxDepth = (int) (TryGetFloatValue(descendants, nameof(MaxDepth)) ?? MaxDepth);
            PerDepth = TryGetFloatValue(descendants, nameof(PerDepth)) ?? PerDepth;
            PerHeight = TryGetFloatValue(descendants, nameof(PerHeight)) ?? PerHeight;
            WhenUnselected = TryGetFloatValue(descendants, nameof(WhenUnselected)) ?? WhenUnselected;
            
            ConnectionWidth = TryGetFloatValue(descendants, nameof(ConnectionWidth)) ?? ConnectionWidth;
            HighlightedConnectionWidth = TryGetFloatValue(descendants, nameof(HighlightedConnectionWidth)) ?? HighlightedConnectionWidth;

            ArrowBaseSize = TryGetFloatValue(descendants, nameof(ArrowBaseSize)) ?? ArrowBaseSize;
            ArrowLength = TryGetFloatValue(descendants, nameof(ArrowLength)) ?? ArrowLength;
            DistanceBetweenArrows = TryGetFloatValue(descendants, nameof(DistanceBetweenArrows)) ?? DistanceBetweenArrows;
            AssemblyModelHeight = TryGetFloatValue(descendants, nameof(AssemblyModelHeight)) ?? AssemblyModelHeight;
            AssemblyModelWidth = TryGetFloatValue(descendants, nameof(AssemblyModelWidth)) ?? AssemblyModelWidth;

            DependenciesSortingGroupThreshold = (int) (TryGetFloatValue(descendants, nameof(DependenciesSortingGroupThreshold)) ?? DependenciesSortingGroupThreshold);

        }

        // in case some new field have been added, we keep the file up to date
        SaveStateToDisk();
    }

    [CanBeNull]
    private static string TryGetStringValue([NotNull] IEnumerable<XElement> descendants, [NotNull] string nodeName)
    {
        return descendants.SingleOrDefault(x => x.Name == nodeName)?.Value;
    }

    private static float? TryGetFloatValue([NotNull] IEnumerable<XElement> descendants, [NotNull] string nodeName)
    {
        string value = TryGetStringValue(descendants, nodeName);
        return float.TryParse(value, out float result) ? result : (float?)null;
    }

    private static void SaveStateToDisk()
    {
        var builder = new StringBuilder();
        var settings = new XmlWriterSettings
        {
            OmitXmlDeclaration = true,
            Indent = true
        };
        
        var fields = typeof(Settings).GetFields()
                              .Where(x => x.IsPublic)
                              .Select(field => new XElement(field.Name, field.GetValue(typeof(Settings))));

        var content = new XElement("Settings", fields);

        using (XmlWriter xw = XmlWriter.Create(builder, settings))
        {
            var doc = new XDocument(content);
            doc.Save(xw);
        }

        File.WriteAllText(GetSettingsFilePath(), builder.ToString());
    }
}