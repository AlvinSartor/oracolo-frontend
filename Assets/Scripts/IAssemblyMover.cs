﻿using System.Collections.Generic;
using Assets.UI;
using JetBrains.Annotations;

namespace Assets.Scripts
{
    internal interface IAssemblyMover
    {
        void ResetHightlightChanges([NotNull] IEnumerable<AssemblyModel> allAssemblies);
    }
}
