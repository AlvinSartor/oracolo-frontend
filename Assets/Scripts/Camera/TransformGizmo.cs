using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Assets.UI;
using JetBrains.Annotations;

namespace RuntimeGizmos
{
    [RequireComponent(typeof(Camera))]
    public class TransformGizmo : MonoBehaviour
    {
        private const float s_ModelWidth = 180;
        private const float s_ModelHeight = 70;

        private readonly Color m_XColor = new Color(1, 0, 0, 0.8f);
        private readonly Color m_YColor = new Color(0, 1, 0, 0.8f);
        private readonly Color m_ZColor = new Color(0, 0, 1, 0.8f);
        private readonly Color m_SelectedColor = new Color(1, 1, 0, 0.8f);
        private readonly Color m_HoverColor = new Color(1, .75f, 0, 0.8f);

        private const float s_HandleLength = .1f;
        private const float s_HandleWidth = .002f;
        private const float s_TriangleSize = .02f;
        private const float s_MinSelectedDistanceCheck = .04f;
        private const float s_MoveSpeedMultiplier = 1f;

        private readonly AxisVectors m_HandleLines = new AxisVectors();
        private readonly AxisVectors m_HandleTriangles = new AxisVectors();

        private bool m_IsTransforming;
        private Axis m_NearAxis = Axis.None;
        private readonly AxisInfo m_AxisInfo = new AxisInfo();
        private readonly AxisVectors m_AxisVectorsBuffer = new AxisVectors();

        [CanBeNull] private Transform Target()
        {
            try
            {
                return m_TargetRootsOrdered?.FirstOrDefault()?.transform;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private List<AssemblyModel> m_TargetRootsOrdered = new List<AssemblyModel>();

        private Vector3 PivotPoint()
        {
            var result = new Vector3();
            Transform target = Target();

            if (target != null)
            {
                result = target.transform.position;
                result.x -= s_ModelWidth * .43f;
                result.y += s_ModelHeight * .2f;
            }

            return result;
        }

        public void AddTarget([NotNull] AssemblyModel target)
        {
            m_TargetRootsOrdered = new List<AssemblyModel> {target};
        }
        public void ClearTargets()
        {
            m_TargetRootsOrdered = new List<AssemblyModel>();
        }
        public void RemoveFromTargets([NotNull] AssemblyModel model)
        {
            // there will be a limited amount of objects in the list (genrally 1)
            // so this is not an expensive operation at all
            if (m_TargetRootsOrdered.Contains(model))
            {
                m_TargetRootsOrdered.Remove(model);
            }
        }

        private Camera m_Camera;
        private static Material s_LineMaterial;

        private void Awake()
        {
            m_Camera = GetComponent<Camera>();
            s_LineMaterial = s_LineMaterial ?? new Material(Shader.Find("Custom/Lines"));
        }

        private void Update()
        {
            SetNearAxis();

            if (!Input.GetMouseButtonDown(0) || Target() == null || m_NearAxis == Axis.None)
            {
                return;
            }

            StartCoroutine(TransformSelected());
        }

        private void LateUpdate()
        {
            if (Target() == null)
            {
                return;
            }

            //We run this in lateupdate since coroutines run after update and we want our gizmos to have the updated target transform position after TransformSelected()
            SetAxisInfo();
            SetLines();
        }

        private void OnPostRender()
        {
            if (Target() == null)
            {
                return;
            }

            s_LineMaterial.SetPass(0);

            Color xColor = m_NearAxis == Axis.X ? m_IsTransforming ? m_SelectedColor : m_HoverColor : m_XColor;
            Color yColor = m_NearAxis == Axis.Y ? m_IsTransforming ? m_SelectedColor : m_HoverColor : m_YColor;
            Color zColor = m_NearAxis == Axis.Z ? m_IsTransforming ? m_SelectedColor : m_HoverColor : m_ZColor;

            //the order here is important
            DrawQuads(m_HandleLines.Z, zColor);
            DrawQuads(m_HandleLines.X, xColor);
            DrawQuads(m_HandleLines.Y, yColor);

            DrawTriangles(m_HandleTriangles.X, xColor);
            DrawTriangles(m_HandleTriangles.Y, yColor);
            DrawTriangles(m_HandleTriangles.Z, zColor);
        }

        [NotNull]
        private IEnumerator TransformSelected()
        {
            m_IsTransforming = true;

            Vector3 originalPivot = PivotPoint();

            Vector3 planeNormal = (transform.position - originalPivot).normalized;
            Vector3 axis = GetNearAxisDirection();
            Vector3 projectedAxis = Vector3.ProjectOnPlane(axis, planeNormal).normalized;
            Vector3 previousMousePosition = Vector3.zero;

            while (!Input.GetMouseButtonUp(0))
            {
                Ray mouseRay = m_Camera.ScreenPointToRay(Input.mousePosition);
                Vector3 mousePosition = Geometry.LinePlaneIntersect(mouseRay.origin, mouseRay.direction, originalPivot, planeNormal);

                if (previousMousePosition != Vector3.zero && mousePosition != Vector3.zero)
                {
                    float moveAmount = ExtVector3.MagnitudeInDirection(mousePosition - previousMousePosition, projectedAxis) * s_MoveSpeedMultiplier;
                    Vector3 movement = axis * moveAmount;

                    foreach (var target in m_TargetRootsOrdered)
                    {
                        target.transform.Translate(movement, Space.World);
                        target.UpdateConnectorLines();
                    }
                }

                previousMousePosition = mousePosition;

                yield return null;
            }

            m_IsTransforming = false;
        }

        private Vector3 GetNearAxisDirection()
        {
            switch (m_NearAxis)
            {
                case Axis.X:
                    return m_AxisInfo.XDirection;
                case Axis.Y:
                    return m_AxisInfo.YDirection;
                case Axis.Z:
                    return m_AxisInfo.ZDirection;
                case Axis.Any:
                    return Vector3.one;
                case Axis.None:
                    return Vector3.zero;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        private void SetNearAxis()
        {
            if (Target() is null || m_IsTransforming)
            {
                return;
            }

            float distanceMultiplier = GetDistanceMultiplier();

            m_AxisVectorsBuffer.Clear();
            float tipMinSelectedDistanceCheck = (s_MinSelectedDistanceCheck + s_TriangleSize) * distanceMultiplier;
            m_AxisVectorsBuffer.Add(m_HandleTriangles);
            m_NearAxis = GetNearest(m_AxisVectorsBuffer, tipMinSelectedDistanceCheck);
        }

        private Axis GetNearest([NotNull] AxisVectors axisVectors, float minSelectedDistanceCheck)
        {
            float xClosestDistance = ClosestDistanceFromMouseToLines(axisVectors.X);
            float yClosestDistance = ClosestDistanceFromMouseToLines(axisVectors.Y);
            float zClosestDistance = ClosestDistanceFromMouseToLines(axisVectors.Z);

            bool IsCloseEnough(float distance) => distance <= minSelectedDistanceCheck
                                                  && distance <= xClosestDistance
                                                  && distance <= yClosestDistance
                                                  && distance <= zClosestDistance;

            if (IsCloseEnough(xClosestDistance))
            {
                return Axis.X;
            }

            if (IsCloseEnough(yClosestDistance))
            {
                return Axis.Y;
            }

            if (IsCloseEnough(zClosestDistance))
            {
                return Axis.Z;
            }

            return Axis.None;
        }

        private float ClosestDistanceFromMouseToLines([NotNull] IReadOnlyList<Vector3> lines)
        {
            Ray mouseRay = m_Camera.ScreenPointToRay(Input.mousePosition);

            float closestDistance = float.MaxValue;
            for (int i = 0; i < lines.Count; i += 2)
            {
                IntersectPoints points = Geometry.ClosestPointsOnSegmentToLine(lines[i], lines[i + 1], mouseRay.origin, mouseRay.direction);
                float distance = Vector3.Distance(points.first, points.second);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                }
            }

            return closestDistance;
        }

        private void SetAxisInfo()
        {
            Transform target;
            if ((target = Target()) is null)
            {
                return;
            }

            m_AxisInfo.Set(target, PivotPoint(), s_HandleLength * GetDistanceMultiplier(), TransformSpace.Global);
        }

        private static float GetDistanceMultiplier()
        {
            return 150f;
            //if (Target == null)
            //{
            //    return 0f;
            //}

            //return m_Camera.orthographic
            //    ? Mathf.Max(.01f, m_Camera.orthographicSize * 2f)
            //    : Mathf.Max(.01f, Mathf.Abs(ExtVector3.MagnitudeInDirection(PivotPoint() - transform.position, m_Camera.transform.forward)));
        }

        private void SetLines()
        {
            SetHandleLines();
            SetHandleTriangles();
        }

        private void SetHandleLines()
        {
            m_HandleLines.Clear();

            float distanceMultiplier = GetDistanceMultiplier();
            float lineWidth = s_HandleWidth * distanceMultiplier;
            //When scaling, the axis will have different line lengths and direction.
            float xLineLength = Vector3.Distance(PivotPoint(), m_AxisInfo.XAxisEnd);
            float yLineLength = Vector3.Distance(PivotPoint(), m_AxisInfo.YAxisEnd);
            float zLineLength = Vector3.Distance(PivotPoint(), m_AxisInfo.ZAxisEnd);

            AddQuads(PivotPoint(), m_AxisInfo.XDirection, m_AxisInfo.YDirection, m_AxisInfo.ZDirection, xLineLength, lineWidth, m_HandleLines.X);
            AddQuads(PivotPoint(), m_AxisInfo.YDirection, m_AxisInfo.XDirection, m_AxisInfo.ZDirection, yLineLength, lineWidth, m_HandleLines.Y);
            AddQuads(PivotPoint(), m_AxisInfo.ZDirection, m_AxisInfo.XDirection, m_AxisInfo.YDirection, zLineLength, lineWidth, m_HandleLines.Z);
        }

        private void SetHandleTriangles()
        {
            m_HandleTriangles.Clear();

            float triangleLength = s_TriangleSize * GetDistanceMultiplier();
            AddTriangles(m_AxisInfo.XAxisEnd, m_AxisInfo.XDirection, m_AxisInfo.YDirection, m_AxisInfo.ZDirection, triangleLength,
                         m_HandleTriangles.X);
            AddTriangles(m_AxisInfo.YAxisEnd, m_AxisInfo.YDirection, m_AxisInfo.XDirection, m_AxisInfo.ZDirection, triangleLength,
                         m_HandleTriangles.Y);
            AddTriangles(m_AxisInfo.ZAxisEnd, m_AxisInfo.ZDirection, m_AxisInfo.YDirection, m_AxisInfo.XDirection, triangleLength,
                         m_HandleTriangles.Z);
        }

        private static void AddTriangles(Vector3 axisEnd, Vector3 axisDirection, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2,
                                         float size, [NotNull] ICollection<Vector3> resultsBuffer)
        {
            Vector3 endPoint = axisEnd + (axisDirection * (size * 2f));
            Square baseSquare = GetBaseSquare(axisEnd, axisOtherDirection1, axisOtherDirection2, size / 2f);

            resultsBuffer.Add(baseSquare.BottomLeft);
            resultsBuffer.Add(baseSquare.TopLeft);
            resultsBuffer.Add(baseSquare.TopRight);
            resultsBuffer.Add(baseSquare.TopLeft);
            resultsBuffer.Add(baseSquare.BottomRight);
            resultsBuffer.Add(baseSquare.TopRight);

            for (int i = 0; i < 4; i++)
            {
                resultsBuffer.Add(baseSquare[i]);
                resultsBuffer.Add(baseSquare[i + 1]);
                resultsBuffer.Add(endPoint);
            }
        }

        private static void AddQuads(Vector3 axisStart, Vector3 axisDirection, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2,
                                     float length, float width, [NotNull] ICollection<Vector3> resultsBuffer)
        {
            Vector3 axisEnd = axisStart + (axisDirection * length);
            AddQuads(axisStart, axisEnd, axisOtherDirection1, axisOtherDirection2, width, resultsBuffer);
        }

        private static void AddQuads(Vector3 axisStart, Vector3 axisEnd, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float width,
                                     [NotNull] ICollection<Vector3> resultsBuffer)
        {
            Square baseRectangle = GetBaseSquare(axisStart, axisOtherDirection1, axisOtherDirection2, width);
            Square baseRectangleEnd = GetBaseSquare(axisEnd, axisOtherDirection1, axisOtherDirection2, width);

            resultsBuffer.Add(baseRectangle.BottomLeft);
            resultsBuffer.Add(baseRectangle.TopLeft);
            resultsBuffer.Add(baseRectangle.TopRight);
            resultsBuffer.Add(baseRectangle.BottomRight);

            resultsBuffer.Add(baseRectangleEnd.BottomLeft);
            resultsBuffer.Add(baseRectangleEnd.TopLeft);
            resultsBuffer.Add(baseRectangleEnd.TopRight);
            resultsBuffer.Add(baseRectangleEnd.BottomRight);

            for (int i = 0; i < 4; i++)
            {
                resultsBuffer.Add(baseRectangle[i]);
                resultsBuffer.Add(baseRectangleEnd[i]);
                resultsBuffer.Add(baseRectangleEnd[i + 1]);
                resultsBuffer.Add(baseRectangle[i + 1]);
            }
        }

        private static Square GetBaseSquare(Vector3 axisEnd, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float size)
        {
            Square square;
            Vector3 offsetUp = ((axisOtherDirection1 * size) + (axisOtherDirection2 * size));
            Vector3 offsetDown = ((axisOtherDirection1 * size) - (axisOtherDirection2 * size));

            //These might not really be the proper directions, as in the bottomLeft might not really be at the bottom left...
            square.BottomLeft = axisEnd + offsetDown;
            square.TopLeft = axisEnd + offsetUp;
            square.BottomRight = axisEnd - offsetUp;
            square.TopRight = axisEnd - offsetDown;
            return square;
        }

        private static void DrawTriangles([NotNull] IReadOnlyList<Vector3> lines, Color color)
        {
            GL.Begin(GL.TRIANGLES);
            GL.Color(color);

            for (int i = 0; i < lines.Count; i += 3)
            {
                GL.Vertex(lines[i]);
                GL.Vertex(lines[i + 1]);
                GL.Vertex(lines[i + 2]);
            }

            GL.End();
        }

        private static void DrawQuads([NotNull] IReadOnlyList<Vector3> lines, Color color)
        {
            GL.Begin(GL.QUADS);
            GL.Color(color);

            for (int i = 0; i < lines.Count; i += 4)
            {
                GL.Vertex(lines[i]);
                GL.Vertex(lines[i + 1]);
                GL.Vertex(lines[i + 2]);
                GL.Vertex(lines[i + 3]);
            }

            GL.End();
        }
    }
}