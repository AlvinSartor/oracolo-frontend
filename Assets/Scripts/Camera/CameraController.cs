﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.UI;
using JetBrains.Annotations;
using RuntimeGizmos;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform m_Target;
    private Vector3 m_DesiredRelativePosition;

    private float m_AngleX;
    private float m_AngleY;
    private float m_Distance;

    private const float s_DefaultDistance = 350f;
    private const float s_MaxDistance = 3000;
    private const float s_MinDistance = 80;

    private const float s_ZoomSpeed = 1200;
    private const float s_PanSpeed = 1000;
    private const float s_RotationSpeed = Mathf.PI / 3;
    private const float s_TranslationSpeed = 3000;

    private void Initialize()
    {
        if (m_Target != null)
        {
            return;
        }

        const float initialDistance = (s_MaxDistance + s_MinDistance) / 2f;
        m_Target = new GameObject("CameraTarget").transform;
        m_Target.transform.position = transform.forward * initialDistance;

        m_Distance = Vector3.Distance(m_Target.position, transform.position);
        m_AngleX = -Mathf.PI / 2;
        m_AngleY = 1.43333f;
    }

    private void Start() => Initialize();

    private void Reset() => Initialize();

    private void LateUpdate()
    {
        MouseInputsHandling();
        TouchInputsHandling();

        Vector3 desiredPosition = CalculateDesiredPosition();
        if (!(Vector3.Distance(desiredPosition, transform.position) > 0.025f))
        {
            return;
        }

        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, s_TranslationSpeed * Time.deltaTime);
        transform.LookAt(m_Target);
    }

    private void MouseInputsHandling()
    {
        if (Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftAlt))
        {
            ApplyRotation(GetMouseAxisValues());
        }

        if (Input.GetMouseButton(2))
        {
            ApplyPan(GetMouseAxisValues());
        }

        float mouseWheel = -Input.GetAxis("Mouse ScrollWheel");
        if (Math.Abs(mouseWheel) > double.Epsilon)
        {
            ApplyZoom(mouseWheel);
        }
    }

    private bool m_TouchZooming;
    private bool m_TouchPanning;
    private bool m_TouchRotating;

    private Vector2 m_TouchPosition;
    private float m_TouchDistance;

    private void TouchInputsHandling()
    {
        HandleTouchZoom();
        HandleTouchPan();
        HandleTouchRotation();
    }

    private void HandleTouchRotation()
    {
        if (Input.touchCount == 3)
        {
            if (!m_TouchRotating)
            {
                m_TouchRotating = true;
                m_TouchPosition = GetTouchCenter();
            }

            Vector2 actualPosition = GetTouchCenter();
            Vector2 delta = actualPosition - m_TouchPosition;
            m_TouchPosition = actualPosition;

            ApplyRotation(delta / 20);
        }
        else
        {
            m_TouchRotating = false;
        }
    }

    private void HandleTouchPan()
    {
        if (Input.touchCount == 1)
        {
            if (!m_TouchPanning)
            {
                m_TouchPanning = true;
                m_TouchPosition = Input.GetTouch(0).position;
            }

            Vector2 actualPosition = Input.GetTouch(0).position;
            Vector2 delta = actualPosition - m_TouchPosition;
            m_TouchPosition = actualPosition;

            ApplyPan(delta / 10);
        }
        else
        {
            m_TouchPanning = false;
        }
    }

    private void HandleTouchZoom()
    {
        if (Input.touchCount == 2)
        {
            if (!m_TouchZooming)
            {
                m_TouchZooming = true;
                m_TouchDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            }

            float actualDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            float deltaDistance = actualDistance - m_TouchDistance;
            m_TouchDistance = actualDistance;

            ApplyZoom(-deltaDistance / 150);
        }
        else
        {
            m_TouchZooming = false;
        }
    }

    private static Vector2 GetTouchCenter()
    {
        List<Vector2> touchPositions = new List<Vector2>();
        for (var i = 0; i < Input.touchCount; i++)
        {
            touchPositions.Add(Input.GetTouch(i).position);
        }

        return touchPositions.Aggregate(new Vector2(0, 0), (act, succ) => act + succ) / touchPositions.Count;
    }

    private static Vector2 GetMouseAxisValues()
    {
        return new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
    }

    private void ApplyRotation(Vector2 rotationAxis)
    {
        m_AngleX = (m_AngleX + s_RotationSpeed * Time.deltaTime * -rotationAxis.x) % (2 * Mathf.PI);
        m_AngleY = (m_AngleY + s_RotationSpeed * Time.deltaTime * rotationAxis.y) % (2 * Mathf.PI);
    }

    private void ApplyPan(Vector2 panAxis)
    {
        //grab the rotation of the camera so we can move in a psuedo local XY space
        m_Target.rotation = transform.rotation;
        m_Target.Translate(Vector3.right * -panAxis.x * s_PanSpeed * Time.deltaTime);
        m_Target.Translate(transform.up * -panAxis.y * s_PanSpeed * Time.deltaTime, Space.World);
    }

    private void ApplyZoom(float zoomValue)
    {
        m_Distance = Mathf.Clamp(m_Distance + zoomValue * s_ZoomSpeed, s_MinDistance, s_MaxDistance);
    }

    private Vector3 CalculateDesiredPosition()
    {
        return new Vector3(Mathf.Cos(m_AngleX) * Mathf.Sin(m_AngleY) * m_Distance + m_Target.position.x,
                           Mathf.Cos(m_AngleY) * m_Distance + m_Target.position.y,
                           Mathf.Sin(m_AngleX) * Mathf.Sin(m_AngleY) * m_Distance + m_Target.position.z);
    }


    /// <summary>
    /// Sets a new target for the camera.
    /// </summary>
    public void SetNewTarget([NotNull] Transform newTarget, bool keepPosition = false)
    {
        m_Target.position = newTarget.transform.position;
        m_Distance = keepPosition 
            ? Vector3.Distance(m_Target.position, transform.position) 
            : s_DefaultDistance;
    }

    /// <summary>
    /// Gets the target position.
    /// </summary>
    public Vector3 GetTargetPosition() => m_Target.position;

    /// <summary>
    /// Deselect all selected models.
    /// <remarks> Used from button.OnClick in editor. </remarks>
    /// </summary>
    public void LooseSelection()
    {
        AssemblyModel.DeselectCurrentSelected();
        GetComponent<TransformGizmo>().ClearTargets();
    }
}