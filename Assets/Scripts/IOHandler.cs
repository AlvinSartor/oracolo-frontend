﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeAnalysis.ProjectMapping.Interfaces;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

public class IOHandler : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] private TMP_Text m_AssemblyNameTooltip;
    [SerializeField] private CsprojInputField m_InputField;
#pragma warning restore 649


    private GraphBuilder m_Builder;
    private const int MinTextLengthToGiveSuggestions = 3;
    private const int NumberOfSuggestions = 10;

    public void Start()
    {
        m_Builder = GetComponent<GraphBuilder>();

        m_AssemblyNameTooltip.text = string.Empty;
        
        m_InputField.OnSubmit = AssemblyRequested;
        m_InputField.OnContentClicked = csproj => AssemblyRequested(csproj.CsprojName);
        m_InputField.GetSuggestions = GetAssemblySuggestions;
    }

    [NotNull]
    private IEnumerable<ICsprojDescriptor> GetAssemblySuggestions([NotNull] string text)
    {
        return text.Length < MinTextLengthToGiveSuggestions 
            ? new ICsprojDescriptor[]{} 
            : m_Builder.Plug.AllCsprojs().Where(x => x.Key.Contains(text)).Select(x => x.Value).Take(NumberOfSuggestions);
    }

    private void AssemblyRequested([NotNull] string assemblyName)
    {
        m_InputField.Empty();

        if (m_Builder.VerifyAssemblyName(assemblyName))
        {
            m_Builder.AddAssemblyUsingName(assemblyName, true);
            ShowMessage(m_AssemblyNameTooltip, $"Assembly {assemblyName} is being added.");
            return;
        }

        ShowMessage(m_AssemblyNameTooltip, $"Assembly {assemblyName} not found.");
    }   

    private void ShowMessage([NotNull] TMP_Text tooltip, [NotNull] string text)
    {
        tooltip.alpha = 1;
        tooltip.text = text;
        StartCoroutine("Fade", tooltip);
    }

    // ReSharper disable once UnusedMember.Local
    [NotNull]
    private IEnumerator Fade([NotNull] TMP_Text tooltip)
    {
        while (tooltip.alpha > 0)
        {
            tooltip.alpha -= Time.deltaTime * 0.33f;
            yield return null;
        }
    }
}