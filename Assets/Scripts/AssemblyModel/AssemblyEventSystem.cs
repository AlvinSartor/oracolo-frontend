﻿using System;
using Assets.UI;
using Extensions;
using JetBrains.Annotations;
using RuntimeGizmos;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AssemblyEventSystem : EventTrigger
{
    private bool m_Initialized;

    [CanBeNull] private GameObject m_DetailsPanel;
    [NotNull] private TMP_Text m_NameText;

    //[NotNull] private Button[] m_Buttons;   
    [NotNull] private AssemblyModel m_Model;

    private float m_LastClickTime;
    [CanBeNull] private static CameraController s_CameraController;
    [CanBeNull] private static TransformGizmo s_CameraGizmos;

    public void Initialize([NotNull] AssemblyModel model)
    {
        m_Initialized = true;

        m_Model = model;

        m_NameText = transform.Find("AssemblyName").GetComponentInChildren<TMP_Text>();
        m_NameText.text = ExtractLastName(m_Model.Name);
    }

    [NotNull]
    private string ExtractLastName([NotNull] string csprojName)
    {
        int indexOfPoint = m_Model.Name.LastIndexOf(".", StringComparison.InvariantCulture);
        return indexOfPoint == 0 ? csprojName : csprojName.Substring(indexOfPoint + 1);
    }

    /// <inheritdoc />
    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (!m_Initialized)
        {
            return;
        }
        
        m_NameText.gameObject.SetActive(false); 
        AddDetails();

        m_Model.StartHighlightConnections();
    }

    /// <inheritdoc />
    public override void OnPointerExit(PointerEventData eventData)
    {
        if (!m_Initialized)
        {
            return;
        }
        
        m_NameText.gameObject.SetActive(true);
        RemoveDetails();

        m_Model.EndHighlightConnections();
    }

    /// <inheritdoc />
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        float clickTime = Time.time;
        if (clickTime - m_LastClickTime < 0.3)
        {
            m_LastClickTime = 0;
            FetchCameraMovement().SetNewTarget(m_Model.transform);
            return;
        }

        m_LastClickTime = clickTime;
        m_Model.Select();
        FetchCameraGizmo().AddTarget(m_Model);
    }

    [NotNull]
    public static CameraController FetchCameraMovement()
    {
        return s_CameraController ?? (s_CameraController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>());
    }

    [NotNull]
    public static TransformGizmo FetchCameraGizmo()
    {
        return s_CameraGizmos ?? (s_CameraGizmos = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<TransformGizmo>());
    }

    private void AddDetails()
    {
        if (m_DetailsPanel != null)
        {
            return;
        }

        m_DetailsPanel = Instantiate(m_Model.DetailsContainerModel, transform);

        if (m_DetailsPanel == null)
        {
            return;
        }

        m_DetailsPanel.transform.Find("Name").GetComponentInChildren<TMP_Text>().text = m_Model.Name;
        m_DetailsPanel.transform.Find("Description").GetComponentInChildren<TMP_Text>().text =  m_Model.Description;

        Transform buttonsPanel = m_DetailsPanel.transform.Find("ButtonsContainer");
        buttonsPanel.transform.Find("ExpandButton").GetComponent<Button>().onClick.AddListener(m_Model.RequestExpansion);
        buttonsPanel.transform.Find("IncomingButton").GetComponent<Button>().onClick.AddListener(() => m_Model.RequestHighlightDependencies(true));
        buttonsPanel.transform.Find("OutcomingButton").GetComponent<Button>().onClick.AddListener(() => m_Model.RequestHighlightDependencies(false));
        buttonsPanel.transform.Find("DeleteButton").GetComponent<Button>().onClick.AddListener(m_Model.RequestRemotion);
    }

    private void RemoveDetails()
    {
        if (m_DetailsPanel == null)
        {
            return;
        }

        m_DetailsPanel.GetComponentsInChildren<Button>().ForEach(x => x.onClick.RemoveAllListeners());

        Destroy(m_DetailsPanel);
        m_DetailsPanel = null;
    }

}
