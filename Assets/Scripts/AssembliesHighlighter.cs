﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using AdvancedGraph.Interfaces;
using Assets.UI;
using CodeAnalysis.ProjectMapping.Interfaces;
using Extensions;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Scripts
{
    public sealed class AssembliesHighlighter : IAssemblyMover
    {
        private readonly Func<ICsprojDescriptor, AssemblyModel> m_GetAssembly;

        private readonly Dictionary<ICsprojDescriptor, Vector3> m_OldPositions;
        private Dictionary<ICsprojDescriptor, int> m_ExaminedNodes;
        private LinkedList<ICsprojDescriptor>[] m_DepthList;
        private AssemblyModel m_Reference;
        private bool m_NeedsReset;

        public AssembliesHighlighter([NotNull] Func<ICsprojDescriptor, AssemblyModel> getAssembly)
        {
            m_GetAssembly = getAssembly;
            m_OldPositions = new Dictionary<ICsprojDescriptor, Vector3>();
        }

        public void SelectiveHighlight(
            [NotNull] ICsprojDescriptor startingPoint, 
            [NotNull] IGraph<ICsprojDescriptor> graph, 
            bool exiting, 
            int depth)
        {
            ResetHightlightChanges(graph.Values.Select(m_GetAssembly));

            m_ExaminedNodes = BuildNodesStructure(startingPoint, graph, depth);
            m_DepthList = ExtractDepthLists(m_ExaminedNodes, depth);
            m_Reference = m_GetAssembly(startingPoint);
            
            graph.Values.ForEach(x => m_GetAssembly(x).SetTransparent());
            DisponeHiglighted(m_Reference.transform.position, exiting);
            DisponeNonHiglighted(m_Reference.transform.position.z, graph.Values.Select(m_GetAssembly));
            m_NeedsReset = true;
        }

        [NotNull]
        private static Dictionary<ICsprojDescriptor, int> BuildNodesStructure(
            [NotNull] ICsprojDescriptor startingPoint,
            [NotNull] IGraph<ICsprojDescriptor> graph,
            int depth)
        {
            int counter = 0;
            var set = startingPoint.Enlist();

            var nodesToHiglight = new Dictionary<ICsprojDescriptor, int>
            {
                {startingPoint, 0}
            };

            while (set.Count > 0 && counter < depth)
            {
                counter++;
                ImmutableList<ICsprojDescriptor> descendants = set.SelectMany(node => graph[node].CurrentNeighbors.Select(x => x.Value))
                                                                  .Where(x => !nodesToHiglight.ContainsKey(x))
                                                                  .Distinct()
                                                                  .ToImmutableList();

                descendants.ForEach(node =>
                {
                    nodesToHiglight.Add(node, counter);
                });

                set = descendants;
            }

            return nodesToHiglight;
        }

        [NotNull]
        private static LinkedList<ICsprojDescriptor>[] ExtractDepthLists([NotNull] Dictionary<ICsprojDescriptor, int> asseblies, int depth)
        {
            var result = new LinkedList<ICsprojDescriptor>[depth];
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = new LinkedList<ICsprojDescriptor>();
            }

            foreach (var pair in asseblies)
            {
                result[pair.Value].AddLast(pair.Key);
            }

            return result;
        }

        public void ResetHightlightChanges([NotNull] IEnumerable<AssemblyModel> allAssemblies)
        {
            if (!m_NeedsReset)
            {
                return;
            }

            foreach (var model in  allAssemblies)
            {
                model.SetVisible(true);
                if (m_OldPositions.ContainsKey(model.Csproj))
                {
                    model.transform.position = m_OldPositions[model.Csproj];
                    model.UpdateConnectorLines();
                }
            }

            m_NeedsReset = false;
        }
     
        private void DisponeHiglighted(Vector3 referencePosition, bool exiting)
        {
            int depthCounter = 0;
            foreach (var csprojList in m_DepthList)
            {
                int elements = csprojList.Count;
                int squareSize = (int) Math.Floor(Math.Sqrt(elements));

                int elementCounter = 0;
                foreach (var descriptor in csprojList.OrderBy(x => x.CsprojName.Split('.').Last()))
                {
                    AssemblyModel model = m_GetAssembly(descriptor);

                    model.SetVisible(exiting);
                    m_OldPositions[model.Csproj] = model.transform.position;

                    Vector3 newPosition = ToWorldCoordinates(GetRowAndColumn(elementCounter, squareSize), squareSize, referencePosition);
                    model.transform.position = newPosition.WithZ(depthCounter * Settings.PerDepth + referencePosition.z);

                    model.UpdateConnectorLines();
                    elementCounter++;
                }
                depthCounter++;
            }         
        }

        private Vector2 GetRowAndColumn(int element, int squareSide)
        {
            int row = element / squareSide;
            int column = element - squareSide * row;
            
            return new Vector2(column, row);
        }

        private Vector2 ToWorldCoordinates(Vector2 columnRow, int squareSide, Vector2 reference)
        {
            Vector2 bounds = Settings.Bounds() * 1.1f;
            Vector2 center = bounds * squareSide / 2f;
            center.y *= -1;
            Vector2 absPosition = new Vector2(columnRow.x * bounds.x, columnRow.y * -bounds.y) - center;

            return absPosition + reference;
        }

        private void DisponeNonHiglighted(float referenceDepth, [NotNull] IEnumerable<AssemblyModel> allModels)
        {
            allModels.Where(node => !m_ExaminedNodes.ContainsKey(node.Csproj))
                        .ForEach(model =>
                        {
                            model.transform.position = model.transform.position.WithZ(Settings.WhenUnselected + referenceDepth);
                            model.UpdateConnectorLines();
                        });
        }
    }
}
