﻿using JetBrains.Annotations;
using UnityEngine;

public static class ImmutablesExtensions
{
    /// <summary>
    /// Returns a copy of the given color with the specified alpha value.
    /// </summary>
    /// <param name="color">The original color.</param>
    /// <param name="alpha">The new alpha value.</param>
    [Pure]
    public static Color WithAlpha(this Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, Mathf.Clamp01(alpha));
    }

    /// <summary>
    /// Returns a copy of the give Vector3 with the specified Z value
    /// </summary>
    /// <param name="position">The original position.</param>
    /// <param name="z">The new z value.</param>    
    [Pure]
    public static Vector3 WithZ(this Vector3 position, float z)
    {
        return new Vector3(position.x, position.y, z);
    }

    /// <summary>
    /// Returns a copy of the give Vector3 with the specified Y value
    /// </summary>
    /// <param name="position">The original position.</param>
    /// <param name="y">The new y value.</param>    
    [Pure]
    public static Vector3 WithY(this Vector3 position, float y)
    {
        return new Vector3(position.x, y, position.z);
    }
}
