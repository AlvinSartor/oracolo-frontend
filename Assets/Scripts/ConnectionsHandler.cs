﻿using System;
using System.Collections.Generic;
using Assets.UI;
using Extensions.Geometry;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Rendering;
using Object = UnityEngine.Object;

public class ConnectionsHandler
{
    [NotNull]
    public static ConnectionsHandler Instance { get; } = new ConnectionsHandler();

    private static Material s_LinesMaterial;
    private static Material s_TransparentMaterial;
    private static Material s_HighlightedMaterial;

    private readonly GameObject m_ConnectionsHolder;
    private readonly Dictionary<string, LineRenderer> m_Connections;
    private readonly Dictionary<string, bool> m_HasTransparentMaterial;
    private readonly HashSet<string> m_IsHighlighted;

    private LineRenderer AddNewConnector(string key)
    {
        var gameObject = new GameObject(key);
        gameObject.transform.parent = m_ConnectionsHolder.transform;
        return gameObject.AddComponent<LineRenderer>();
    }

    private ConnectionsHandler()
    {
        s_LinesMaterial = new Material(Resources.Load<Shader>("BlackLine"));
        s_TransparentMaterial = new Material(Resources.Load<Shader>("TransparentLine"));
        s_HighlightedMaterial = Resources.Load<Material>("HighlightedMaterial");

        m_ConnectionsHolder = new GameObject("ConnectionsHolder");       
        m_Connections = new Dictionary<string, LineRenderer>();
        m_HasTransparentMaterial = new Dictionary<string, bool>();
        m_IsHighlighted = new HashSet<string>();
    }

    [NotNull]
    private static string GetKey([NotNull] AssemblyModel s, [NotNull] AssemblyModel d) => $"{s.Csproj.CsprojName}:{d.Csproj.CsprojName}";

    [NotNull]
    private static Vector3[] GetConnectionLine([NotNull] Component s, [NotNull] Component d)
    {
        (Vector3 start, Vector3 end) = Geometry.GetConnectionPointsBetweenRectangles(s.transform.position, d.transform.position, Settings.Bounds());
        return ComposeConnectionLine(start, end);
    }
    
    [NotNull]
    private static Vector3[] ComposeConnectionLine(Vector3 start, Vector3 end)
    {
        float distance = Vector3.Distance(start, end);
        float numArrows = (int)Math.Floor(distance / Settings.DistanceBetweenArrows);
        float arrowsDistance = distance / numArrows;

        List<Vector3> connectionLine = new List<Vector3> { start };

        void AddToPointsList((Vector3, Vector3, Vector3, Vector3) arrow)
        {
            connectionLine.Add(arrow.Item1);
            connectionLine.Add(arrow.Item2);
            connectionLine.Add(arrow.Item3);
            connectionLine.Add(arrow.Item4);
        }

        for (var i = 0; i < numArrows; i++)
        {
            Vector3 arrowPoint = Vector3.Lerp(start, end, (i + 1) * arrowsDistance / distance);
            AddToPointsList(Geometry.GetArrow(start, arrowPoint, Settings.ArrowBaseSize, Settings.ArrowLength));
        }

        return connectionLine.ToArray();
    }
    
    internal void RecalculateConnection([NotNull] AssemblyModel source, [NotNull] AssemblyModel destination)
    {
        string key = GetKey(source, destination);
        if (!m_Connections.ContainsKey(key))
        {
            throw new ArgumentException($"Connection {key} not found.");
        }

        UpdateConnectorLines(source, destination, m_Connections[key]);
    }

    internal void RemoveConnection([NotNull] AssemblyModel source, [NotNull] AssemblyModel destination)
    {
        string key = GetKey(source, destination);
        if (!m_Connections.ContainsKey(key))
        {
            throw new ArgumentException($"Connection {key} not found.");
        }

        var connector = m_Connections[key];
        m_Connections.Remove(key);

        Object.Destroy(connector.gameObject);
    }

    internal void AddConnection([NotNull] AssemblyModel source, [NotNull] AssemblyModel destination)
    {
        string key = GetKey(source, destination);
        if (m_Connections.ContainsKey(key))
        {
            throw new ArgumentException($"Connection {key} already existing.");
        }

        LineRenderer connector = AddNewConnector(key);
        InitalizeConnector(connector);
        UpdateConnectorLines(source, destination, connector);
        m_Connections.Add(key, connector);
    }

    private static void UpdateConnectorLines([NotNull] Component source, [NotNull] Component destination, [NotNull] LineRenderer connector)
    {
        Vector3[] connectionLine = GetConnectionLine(source, destination);
        connector.positionCount = connectionLine.Length;
        connector.SetPositions(connectionLine);
    }

    private void InitalizeConnector(LineRenderer connector)
    {
        connector.material = s_LinesMaterial;
        connector.receiveShadows = false;
        connector.shadowCastingMode = ShadowCastingMode.Off;
        connector.shadowBias = 0;
        connector.allowOcclusionWhenDynamic = true;

        connector.startColor = new Color(0, 0, 0, 0.5f);
        connector.endColor = new Color(0, 0, 0, 0.5f);

        connector.startWidth = Settings.ConnectionWidth;
        connector.endWidth = Settings.ConnectionWidth;
    }

    internal void StartHighlightConnection([NotNull] AssemblyModel source, [NotNull] AssemblyModel destination)
    {
        string key = GetKey(source, destination);
        if (!m_Connections.ContainsKey(key))
        {
            throw new ArgumentException($"Connection {key} does not exist.");
        }

        if (!m_IsHighlighted.Contains(key))
        {
            m_IsHighlighted.Add(key);
        }
        m_Connections[key].material = s_HighlightedMaterial;
        m_Connections[key].startWidth = Settings.HighlightedConnectionWidth;
        m_Connections[key].endWidth = Settings.HighlightedConnectionWidth;
    }

    internal void EndHighlightConnection([NotNull] AssemblyModel source, [NotNull] AssemblyModel destination)
    {
        string key = GetKey(source, destination);
        if (!m_Connections.ContainsKey(key))
        {
            throw new ArgumentException($"Connection {key} does not exist.");
        }

        if (m_IsHighlighted.Contains(key))
        {
            m_IsHighlighted.Remove(key);
            m_Connections[key].material = m_HasTransparentMaterial.TryGetValue(key, out bool res) && res 
                ? s_TransparentMaterial
                : s_LinesMaterial;
        }

        m_Connections[key].startWidth = Settings.ConnectionWidth;
        m_Connections[key].endWidth = Settings.ConnectionWidth;       
    }

    public void SetTransparent([NotNull] AssemblyModel source, [NotNull] AssemblyModel destination)
    {
        string key = GetKey(source, destination);
        if (!m_Connections.ContainsKey(key))
        {
            throw new ArgumentException($"Connection {key} does not exist.");
        }

        m_HasTransparentMaterial[key] = true;
        if (!m_IsHighlighted.Contains(key))
        {
            m_Connections[key].material = s_TransparentMaterial;
        }
    }

    public void SetVisible([NotNull] AssemblyModel source, [NotNull] AssemblyModel destination)
    {
        string key = GetKey(source, destination);
        if (!m_Connections.ContainsKey(key))
        {
            throw new ArgumentException($"Connection {key} does not exist.");
        }

        m_HasTransparentMaterial[key] = false;
        if (!m_IsHighlighted.Contains(key))
        {
            m_Connections[key].material = s_LinesMaterial;
        }
    }
}