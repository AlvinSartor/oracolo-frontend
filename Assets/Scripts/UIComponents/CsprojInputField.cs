﻿using System;
using System.Collections.Generic;
using CodeAnalysis.ProjectMapping.Interfaces;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

public class CsprojInputField : InputFieldWithSuggestions<ICsprojDescriptor>
{
    private const int MinTextLengthToGiveSuggestions = 3;
    private const int NumberOfSuggestions = 10;

    [SerializeField, NotNull] public GameObject ItemTemplate;
    [NotNull] public Action<string> OnSubmit{ get; set; } = text => { };
    [NotNull] public Action<ICsprojDescriptor> OnContentClicked { get; set; } = csproj => { };
    [NotNull] public Func<string, IEnumerable<ICsprojDescriptor>> GetSuggestions { get; set; } = s => new ICsprojDescriptor[]{};

    /// <inheritdoc />
    protected override ContentAdder<ICsprojDescriptor> InstantiateAdder()
    {
        return new ContentAdder<ICsprojDescriptor>(TemplateTransform, csproj => csproj.CsprojName, OnContentClicked, ItemTemplate);
    }

    /// <inheritdoc />
    protected override IEnumerable<ICsprojDescriptor> GetSuggestionsFromText(string inputText)
    {
        return GetSuggestions(inputText);
    }   
}
