﻿using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

    [RequireComponent(typeof(TMP_InputField))]
public abstract class InputFieldWithSuggestions<T> : MonoBehaviour
{   
    private ContentAdder<T> m_ContentAdder = null;
    private TMP_InputField m_InputField;

    [NotNull]
    protected abstract ContentAdder<T> InstantiateAdder();

    [NotNull]
    private ContentAdder<T> GetContentAdder() => m_ContentAdder ?? (m_ContentAdder = InstantiateAdder());

    [NotNull]
    protected abstract IEnumerable<T> GetSuggestionsFromText([NotNull] string text);

    [NotNull]
    protected Transform TemplateTransform => transform.Find("Template");

    private void Start()
    {
        GetComponent<TMP_InputField>().onValueChanged.AddListener(s => AddSuggestionsToContent(GetSuggestionsFromText(s)));
    }

    private void AddSuggestionsToContent([NotNull] IEnumerable<T> suggestions) => GetContentAdder().AddOrSubstituteEntries(suggestions);

    private void OnDestroy()
    {
        GetComponent<TMP_InputField>().onValueChanged.RemoveAllListeners();
    }

    public void Empty()
    {
        GetComponent<TMP_InputField>().text = string.Empty;
        GetContentAdder().Empty();
    }
}