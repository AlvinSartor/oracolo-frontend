﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.UI;
using CodeAnalysis.ProjectMapping.Interfaces;
using JetBrains.Annotations;
using UnityEngine;

public class AssembliesSorter : IAssemblyMover
{
    private readonly Func<ICsprojDescriptor, AssemblyModel> m_GetAssembly;
    private readonly Dictionary<ICsprojDescriptor, Vector3> m_OldPositions;
    private readonly HashSet<int> m_ClustersFound;
    private bool m_NeedsReset;

    /// <summary>
    /// Creates a new instance of <see cref="AssembliesSorter"/>.
    /// </summary>
    /// <param name="getAssembly">The function used to get a model from a CsprojDescriptor.</param>
    public AssembliesSorter([NotNull] Func<ICsprojDescriptor, AssemblyModel> getAssembly)
    {
        m_GetAssembly = getAssembly;
        m_OldPositions = new Dictionary<ICsprojDescriptor, Vector3>();
        m_ClustersFound = new HashSet<int>();
    }

    public void SortByDependencies(
        [NotNull] IEnumerable<AssemblyModel> allAssemblies,
        [NotNull] Func<ICsprojDescriptor, int> getNumberDependencies, 
        Vector3 center)
    {
        Dictionary<ICsprojDescriptor, int> clusters = BuildNodesStructure(allAssemblies, getNumberDependencies);
        Dictionary<ICsprojDescriptor, int> compactedClusters = Compact(clusters); 
        LinkedList<ICsprojDescriptor>[] heightLists = ExtractHeightLists(compactedClusters);
        DisponeClusters(center, heightLists);

        m_NeedsReset = true;
    }

    [NotNull]
    private Dictionary<ICsprojDescriptor, int> Compact([NotNull] Dictionary<ICsprojDescriptor, int> clusters)
    {
        int[] compacted = m_ClustersFound.OrderBy(x => x).ToArray();
        var map = compacted.Select((val, index) => (val, index)).ToDictionary(tuple => tuple.val, tuple => tuple.index);
        return clusters.ToDictionary(pair => pair.Key, pair => map[pair.Value]);
    }

    public void ResetHightlightChanges(IEnumerable<AssemblyModel> allAssemblies)
    {
        if (!m_NeedsReset)
        {
            return;
        }

        foreach (var model in allAssemblies)
        {            
            if (m_OldPositions.ContainsKey(model.Csproj))
            {
                model.transform.position = m_OldPositions[model.Csproj];
                model.UpdateConnectorLines();
            }
        }

        m_NeedsReset = false;
    }

    [NotNull]
    private Dictionary<ICsprojDescriptor, int> BuildNodesStructure(
        [NotNull] IEnumerable<AssemblyModel> allAssemblies, 
        [NotNull] Func<ICsprojDescriptor, int> getNumberDependencies)
    {
        m_ClustersFound.Clear();
        return allAssemblies.ToDictionary(
            assembly => assembly.Csproj,
            assembly =>
            {
                int dependencies = getNumberDependencies(assembly.Csproj);
                int group = AssignToGroup(dependencies);
                m_ClustersFound.Add(group);
                return group;
            });
    }

    private static int AssignToGroup(int numberDependencies) => numberDependencies / Settings.DependenciesSortingGroupThreshold;

    [NotNull]
    private LinkedList<ICsprojDescriptor>[] ExtractHeightLists([NotNull] Dictionary<ICsprojDescriptor, int> asseblies)
    {
        var result = new LinkedList<ICsprojDescriptor>[m_ClustersFound.Count];
        for (var i = 0; i < result.Length; i++)
        {
            result[i] = new LinkedList<ICsprojDescriptor>();
        }

        foreach (var pair in asseblies)
        {
            result[pair.Value].AddLast(pair.Key);
        }

        return result;
    }


    private void DisponeClusters(Vector3 center, [NotNull] LinkedList<ICsprojDescriptor>[] heightLists)
    {       
        int heightCounter = 0;
        foreach (var csprojList in heightLists)
        {
            int elements = csprojList.Count;
            int squareSize = (int)Math.Floor(Math.Sqrt(elements));

            int elementCounter = 0;
            foreach (var descriptor in csprojList.OrderBy(x => x.CsprojName.Split('.').Last()))
            {
                AssemblyModel model = m_GetAssembly(descriptor);

                m_OldPositions[model.Csproj] = model.transform.position;

                //Vector3 newPosition = ToWorldCoordinates(GetRowAndColumn(elementCounter, squareSize), squareSize, center);
                model.transform.position = model.transform.position.WithY(heightCounter * Settings.PerHeight + center.y);

                model.UpdateConnectorLines();
                elementCounter++;
            }
            heightCounter++;
        }
    }
}
