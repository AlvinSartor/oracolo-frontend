# Oracolo

Oracolo aims to become one of those must-have tools that allow a quick and effective visual analysis of large codebases.   
At the moment it still finds itself in an embryo stage, building solid bases to prepare it for the huge responsibilities a program like this must face.   


# Preview
![preview](ReadmeImages/Oracolo.gif)

# Content

The front-end part of the application is a Unity3d project.